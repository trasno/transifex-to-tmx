#!/usr/bin/env python3

import os
from argparse import ArgumentParser
from configparser import ConfigParser
from pathlib import Path
from shutil import rmtree
from subprocess import PIPE, run

import lxml.etree as etree
from transifex.api import transifex_api
from transifex.api.jsonapi.exceptions import DoesNotExist


def clean_1_4(tree, file_name, *, language):
    seen = set()
    body = tree.xpath("//body")[0]
    for unit in tree.xpath("//tu"):
        source_tuv = unit.xpath(
            ".//tuv[contains(@xml:lang, 'en') or contains(@xml:lang, 'EN')]"
        )[0]
        source_tuv.attrib["{http://www.w3.org/XML/1998/namespace}lang"] = "en"
        source = "".join(source_tuv.xpath("seg/text()"))
        try:
            target_tuv = unit.xpath(".//tuv[@xml:lang!='en']")[0]
        except IndexError:
            body.remove(unit)
            continue
        target = "".join(target_tuv.xpath("seg/text()"))
        pair = (source, target)
        if pair not in seen:
            target_tuv.attrib["{http://www.w3.org/XML/1998/namespace}lang"] = language
            seen.add(pair)
            continue
        body.remove(unit)
    if tree.xpath("//tu"):
        with open(file_name, "w") as output_file:
            output_file.write(
                etree.tostring(
                    tree,
                    method="xml",
                    doctype=(
                        '<?xml version="1.0" encoding="UTF-8"?>\n'
                        '<!DOCTYPE tmx SYSTEM "tmx14.dtd">'
                    ),
                    encoding="unicode",
                ),
            )
            output_file.write("\n")
    else:
        os.unlink(file_name)


def clean_1_1(tree, file_name, *, language):
    seen = set()
    body = tree.xpath("//body")[0]
    for unit in tree.xpath("//tu"):
        source_tuv = unit.xpath(
            ".//tuv[contains(@lang, 'en') or contains(@lang, 'EN')]"
        )[0]
        source_tuv.attrib["lang"] = "en"
        source = source_tuv.xpath("seg/text()")[0]
        try:
            target_tuv = unit.xpath(".//tuv[@lang!='en']")[0]
        except IndexError:
            body.remove(unit)
            continue
        target = target_tuv.xpath("seg/text()")[0]
        pair = (source, target)
        if pair not in seen:
            target_tuv.attrib["lang"] = language
            seen.add(pair)
            continue
        body.remove(unit)
    if tree.xpath("//tu"):
        with open(file_name, "w") as output_file:
            output_file.write(
                etree.tostring(
                    tree,
                    method="xml",
                    doctype=(
                        '<?xml version="1.0" encoding="UTF-8"?>\n'
                        '<!DOCTYPE tmx SYSTEM "tmx11.dtd">'
                    ),
                    encoding="unicode",
                ),
            )
            output_file.write("\n")
    else:
        os.unlink(file_name)


CLEANERS = {
    "1.4": clean_1_4,
    "1.1": clean_1_1,
}


def clean(file_name, *, language):
    with open(file_name) as input_file:
        tree = etree.parse(input_file)
    tmx_version = tree.xpath("//tmx")[0].attrib["version"]
    CLEANERS[tmx_version](tree, file_name, language=language)


def download_from_transifex(org, *, language, api_key, dest=None, needs_xliff=False):
    if dest is None:
        dest = org

    config = "[main]\n" "host = https://app.transifex.com\n" "\n"

    transifex_api.setup(auth=api_key)
    try:
        organization = transifex_api.Organization.get(slug=org)
    except DoesNotExist:
        raise RuntimeError(
            f"Non se atopou a organización {org} en Transifex. Igual ten que "
            f"unirse a ela primeiro para ter acceso?"
        )

    rmtree(dest, ignore_errors=True)
    os.mkdir(dest)
    os.mkdir(f"{dest}/.tx")

    projects = organization.fetch("projects")
    for project in projects:
        resources = project.fetch("resources")
        for resource in resources:
            i18n_type = resource.attributes["i18n_type"]
            if i18n_type == "PO":
                extension = "po"
            elif i18n_type == "QT":
                extension = "ts"
            else:
                print(f"Warning: Ignoring resource of unsupported type: {i18n_type}")
                continue
            config += (
                f"[{resource.id}]\n"
                f"file_filter = {resource.attributes['slug']}.{extension}\n"
                f"resource_name = {resource.attributes['name']}\n"
                "\n"
            )
    with open(f"{dest}/.tx/config", "w") as output:
        output.write(config)

    # Algúns proxectos, como Nextcloud, necesitan forzar a descarga a XLIFF
    # porque senón descargan algúns ficheiros en formato distinto de PO.
    if needs_xliff:
        run(["tx", "pull", "-l", language, "--xliff"], cwd=dest)
        result = run(["find", ".", "-name", "*.xlf"], cwd=dest, stdout=PIPE)
        for line in result.stdout.splitlines():
            source = line.decode()[2:]
            target = source[:-4]
            run(["xliff2po", source, target], cwd=dest)
            run(["sed", "-i", "/^#,/d", target], cwd=dest)
            os.unlink(f"{dest}/{source}")
    else:
        run(["tx", "pull", "-l", language], cwd=dest)
        result = run(["find", ".", "-name", "*.ts"], cwd=dest, stdout=PIPE)
        for line in result.stdout.splitlines():
            source = line.decode()[2:]
            target = source[:-3] + ".po"
            run(["ts2po", source, target], cwd=dest)
            os.unlink(f"{dest}/{source}")

    run(["po2tmx", dest, f"{dest}.tmx", "-l", language])
    rmtree(dest)


def main():
    parser = ArgumentParser(
        prog="get",
        description="Download a Transifex project as a TMX file.",
    )
    parser.add_argument("slug")
    parser.add_argument("-l", "--language")
    parser.add_argument("-t", "--api-token")
    args = parser.parse_args()

    api_key = args.api_token
    if not api_key:
        ini = ConfigParser()
        ini.read(Path.home() / ".transifexrc")
        api_key = ini["https://app.transifex.com"]["token"]

    download_from_transifex(args.slug, language=args.language, api_key=api_key)
    clean(f"{args.slug}.tmx", language=args.language)


if __name__ == "__main__":
    main()

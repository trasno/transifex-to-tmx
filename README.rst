================
transifex-to-tmx
================

Install the latest `Transifex client`_.

.. _Transifex client: https://github.com/transifex/cli

Create a text file at ``.transifexrc`` with the following contents, replacing
``<API_TOKEN>`` with your actual `Transifex API token`_::

    [https://app.transifex.com]
    rest_hostname = https://rest.api.transifex.com
    token         = <API_TOKEN>

    [https://www.transifex.com]
    api_hostname = https://api.transifex.com
    hostname     = https://www.transifex.com
    username     = api
    password     = <API_TOKEN>
    token        = <API_TOKEN>

Prepare a Python virtual environment::

    python3 -m venv venv
    . venv/bin/activate
    pip install -r requirements.txt

And then run the ``./get.py`` script specifying the slug of the target
Transifex project (and mind that you must be part of the project), as well as
the target language with the ``-l`` command-line switch, and your `Transifex
API token`_ with the ``-t`` command-line switch.
For example::

    ./get.py sphinx-doc -l gl -t API_TOKEN

This would generate a ``sphinx-doc.tmx`` file.

.. _Transifex API token: https://app.transifex.com/user/settings/api/
